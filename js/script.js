var close_menu = document.querySelector('.burguer_close');
var open_menu = document.querySelector('.burguer_open');
var menu_mobile = document.querySelector('.menu_mobile');
var toggle_menu = document.querySelector('.burguer_open');

close_menu.addEventListener('click', function(){
    menu_mobile.style.display = 'none';
    toggle_menu.classList.toggle("alteranador_menu");
    close_menu.style.display = 'none';
});
open_menu.addEventListener('click', function(){
    menu_mobile.style.display = 'block';
    toggle_menu.classList.toggle("alteranador_menu");
    close_menu.style.display = 'block';
});

(function () {
    var menu = document.querySelector('body>header'); // colocar em cache
    window.addEventListener('scroll', function () {
        if (window.scrollY > 20) menu.classList.add('scrollmenu'); // > 0 ou outro valor desejado
        else menu.classList.remove('scrollmenu');
    });
})();
